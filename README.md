# docstring-workshop

This is the repository for the docstring/code documentation workshop. It shows the benefits of following https://www.python.org/dev/peps/pep-0257/
and how we can integrate it easily into our workflow.