class Polynomial:
    def __init__(self, *coeffs):
        """Return instance of class polynomial
        
        Keyword arguments:
        coeffs -- iterable of coefficients
        """
        self.coeffs = coeffs
        
    def __repr__(self):
        return "Polynomial"+str(self.coeffs)
        
    def __add__(self, other):
        """Adds the Polynomial with another one and returns the result"""
        return Polynomial(*(x+y for x, y in zip(self.coeffs, other.coeffs)))
        
    def __mul__(self, other):
        return Polynomial(*(x*y for x, y in zip(self.coeffs, other.coeffs)))